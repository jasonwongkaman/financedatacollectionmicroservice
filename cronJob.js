const cron = require('node-cron');
const axios = require('axios');
let currentJSON = 1;

const cronJob = cron.schedule("0 0 * * * *", function(){
    console.log('execute started on ' + new Date());
    let currencySet = [{"fromCurr":"AUD", "toCurr":"HKD"},{"fromCurr":"USD", "toCurr":"HKD"},{"fromCurr":"CNY", "toCurr":"HKD"},{"fromCurr":"USD", "toCurr":"CNY"}]
    let currencySet2 = [{"fromCurr":"CAD", "toCurr":"HKD"},{"fromCurr":"EUR", "toCurr":"HKD"},{"fromCurr":"GBP", "toCurr":"HKD"},{"fromCurr":"JPY", "toCurr":"HKD"}]
    

    if(currentJSON == 1){
        console.log("currentJSON is 1. currentSet will be used.");
        for(let i=0;i<currencySet.length;i++){
            console.log(currencySet[i].fromCurr + ": " + currencySet[i].toCurr);
            callExchangeRateData(currencySet[i].fromCurr, currencySet[i].toCurr);
        }
       currentJSON = 2;
    }
    else{
        console.log("currentJSON is 2. currentSet2 will be used.");
        for(let i=0;i<currencySet2.length;i++){
            console.log(currencySet2[i].fromCurr + ": " + currencySet2[i].toCurr);
            callExchangeRateData(currencySet2[i].fromCurr, currencySet2[i].toCurr);
        }
        currentJSON = 1;
    }
    console.log('execute finished on ' + new Date());
})

const callExchangeRateData = (fromCurr, toCurr) =>{
    return new Promise((resolve,error) =>{
        //let financeAPI = `http://localhost:3001/getexchangerate/${currencySet[i].fromCurr}/${currencySet[i].toCurr}`;
        let financeAPI = `http://localhost:3001/getexchangerate/${fromCurr}/${toCurr}`;
        axios.get(financeAPI).then(function (response){
           console.log(fromCurr + ": " + toCurr + " exchange rate query is done");
           Promise.resolve(response.data);
        })
        .catch(function (error){
            console.error('error',error);
            Promise.reject(error);
        });
    }).catch(function (err){
        console.error('error',err);
        Promise.reject(err);
    })

}