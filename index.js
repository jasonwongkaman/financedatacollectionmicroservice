var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

var api = require('./src/api/api.js')(app);

var server = app.listen(3001,function(){
    console.log("Listening on port %s...", server.address().port);
})