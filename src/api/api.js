const financeservice = require('../service/financedataservice');
const axios = require('axios');
const status = require('http-status');

module.exports = (app) => {

    app.get('/getexchangerate/:fromCurr/:toCurr', (request, response) => {
        console.log('Start getting exchange rate.');
        let fromCurr = request.params.fromCurr;
        let toCurr = request.params.toCurr;

        financeservice.getExchangeRate(fromCurr,toCurr).then( result =>{
            response.status(status.OK).send(result);
        });

    })
}