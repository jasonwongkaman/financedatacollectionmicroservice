const sql = require('mssql/msnodesqlv8');
const axios = require('axios');

const config = {
    connectionString: 'Driver=SQL Server;Server=MADHATTER\\MADHATTER;Database=financedata;Trusted_Connection=true;'
};


const getExchangeRate = (fromCurr, toCurr) =>{
    const API_URL = `https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=${fromCurr}&to_currency=${toCurr}&apikey=5HPPWH2GLYWV5GH6`;
    //console.log('API_URL: ' + API_URL);
    let exchangeRateData = {"FromCurrencyCode":"","FromCurrencyName":"","ToCurrencyCode":"","ToCurrencyName":"","ExchangeRate":"","LastRefresh":"","TimeZone":""};
    return new Promise((resolve, error) => {
        axios.get(API_URL).then(function (response){
            //console.log('response data: ' + response.data);
            //console.log('exchange rate data: ' + JSON.stringify(response.data));
            let strJSON = JSON.stringify(response.data["Realtime Currency Exchange Rate"]);
            console.log("response.data: '"+JSON.stringify(response.data) +"'");
            console.log("strJSON: '" + strJSON +"'");
            if(strJSON != null){
                let obj = JSON.parse(strJSON);
                exchangeRateData.FromCurrencyCode = obj["1. From_Currency Code"];
                exchangeRateData.FromCurrencyName = obj["2. From_Currency Name"];
                exchangeRateData.ToCurrencyCode = obj["3. To_Currency Code"];
                exchangeRateData.ToCurrencyName = obj["4. To_Currency Name"];
                exchangeRateData.ExchangeRate = obj["5. Exchange Rate"];
                exchangeRateData.LastRefresh = obj["6. Last Refreshed"];
                exchangeRateData.TimeZone = obj["7. Time Zone"];
                console.log(JSON.stringify(exchangeRateData));
    
                let query = 
                `INSERT INTO [RTExchangeRate]
                ([FromCurrencyCode]
                ,[FromCurrencyName]
                ,[ToCurrencyCode]
                ,[ToCurrencyName]
                ,[ExchangeRate]
                ,[LastRefreshed]
                ,[TimeZone]
                ,[DateCreate])
                VALUES
                ('${exchangeRateData.FromCurrencyCode}'
                ,'${exchangeRateData.FromCurrencyName}'
                ,'${exchangeRateData.ToCurrencyCode}'
                ,'${exchangeRateData.ToCurrencyName}'
                ,'${exchangeRateData.ExchangeRate}'
                ,'${exchangeRateData.LastRefresh}'
                ,'${exchangeRateData.TimeZone}'
                ,GETDATE())`;
                let dbConn = new sql.ConnectionPool(config);
                dbConn.connect().then(function(){
                    let transaction = new sql.Transaction(dbConn);
                    transaction.begin().then(function(){
                        var request = new sql.Request(transaction);
                        request.query(query).then(function(){
                            transaction.commit().then(function(recordSet){
                                console.log("1 record inserted.");
                                dbConn.close();
                            }).catch(function(err){
                                console.log("Error in Transaction Commit " + err);
                                dbConn.close();
                            })
                        }).catch(function (err){
                            console.log("Error in Transaction Begin " + err);
                            dbConn.close();
                        })
                    }).catch(function (err){
                        console.log(err);
                        dbConn.close();
                    })
    
                }).catch(function (err){
                    console.log(err);
                })
                resolve(exchangeRateData);
            }
            else{
                console.log("strJSON is null.");
                resolve(response.data);
            }


        })
        .catch(function (error){
            console.error('error', JSON.stringify(error));
            Promise.reject(error);
            sql.close();
        });
    });


}
module.exports = Object.assign({},{getExchangeRate})